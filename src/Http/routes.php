<?php 

Route::get('/', function() {
	return view('dockermanager::yim');
});

Route::get('container', [
    'as' 	=> 'docker.container', 
    'uses' 	=> 'yimOHG\DockerManager\Http\Controllers\DockerController@showContainer'
]);

Route::get('run/{action}/{id}', [
    'as' 	=> 'docker.run', 
    'uses' 	=> 'yimOHG\DockerManager\Http\Controllers\DockerController@getAction'
]);