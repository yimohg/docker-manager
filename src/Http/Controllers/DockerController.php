<?php

namespace yimOHG\DockerManager\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Docker\Docker;
use Docker\DockerClient;
use Docker\API\Model\ExecConfig;
use Docker\API\Model\ExecStartConfig;

class DockerController extends Controller
{
    public function showContainer()
    {
        try {
            $client = new DockerClient([
                'remote_socket' => env('DOCKER_HOST', 'unix:///var/run/docker.sock'),
                'ssl'           => env('DOCKER_SSL', false),
            ]);
            $docker = new Docker($client);
            $containerManager = $docker->getContainerManager()->findAll(['all' => true]);
   
        } catch( \Exception $e) {
        	$containerManager = [];
            debug($e);
        }

        return view('dockermanager::container', ['containers' => $containerManager]);
    }

    public function getAction($action, $id) {

        try {
            $client = new DockerClient([
                'remote_socket' => env('DOCKER_HOST', 'unix:///var/run/docker.sock'),
                'ssl'           => env('DOCKER_SSL', false),
            ]);
            $docker = new Docker($client);
            $containerManager = $docker->getContainerManager()->$action($id);
        } catch(\Exception $e) {
            debug($e);
        }

        return redirect()->route('docker.container');

    }
}
