@extends('dockermanager::layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">&nbsp;</div>
    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="jumbotron text-center">
                <h1>
                    <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span>
                </h1>

                <h2>
                    <a href="{{ route('docker.container')}}">Docker Container</a>
                </h2>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="jumbotron text-center">
                <h1>
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </h1>

                <h2>
                    <a href="{{ route('robot.testing')}}">Test Cases</a>
                </h2>
            </div>
        </div>

    </div>

@endsection